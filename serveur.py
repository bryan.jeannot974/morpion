from flask import Flask
from flask import jsonify
from flask import request
from flask import redirect



app = Flask(__name__)




nbJoueurs = 0
plateau = [
    '', '', '',
    '', '', '',
    '', '', ''
] #0,#1,#2,#3,#4,#5,#6,#7,#8
joueurQuiDoitJouer = "X"


@app.route('/')
def index():
    return redirect('static/index.html')


@app.route('/rejoindrePartie')
def joindrePartie():
    global nbJoueurs

    if (nbJoueurs >= 2):
        return jsonify({
            "entreeAcceptee": False
        })

    nbJoueurs += 1
    lettres = "XO"
    return jsonify({
        "entreeAcceptee": True,
        "lettre": lettres[nbJoueurs - 1]
    })
        

@app.route('/etatPartie')
def etatPartie():
    global plateau
    global joueurQuiDoitJouer

    return jsonify({
        "plateau": plateau,
        "joueurQuiDoitJouer": joueurQuiDoitJouer
    })


def estPartieFinie():
    global plateau

    def verifier(posBase, decalageCase):
        return plateau[posBase] != "" and plateau[posBase] == plateau[posBase + decalageCase] and plateau[posBase] == plateau[posBase + 2 * decalageCase]


    VerificationsDeplacement = [
        #i, j 
        (0, 1), # Ligne du haut
        (0, 3), # Colone de gauche
        (3, 1), # Ligne du milieu
        (1, 3), # Colone du milieu
        (6, 1), # Ligne du bas
        (2, 3), # Colone de droite
        (0, 4), # Diagonale \
        (2, 2) # Diagonale /
    ]


    gagne = [verifier(posBase, decalageCase) for (posBase, decalageCase) in VerificationsDeplacement]
    if any(gagne):
        return True

    return '' not in plateau 

def changerJoueur():
    global joueurQuiDoitJouer
    joueurQuiDoitJouer = "O" if joueurQuiDoitJouer == "X" else "X"


@app.route('/jouerCoup', methods={'POST'})
def jouerCoup():
    global joueurQuiDoitJouer
    global plateau

    bodyRqst = request.get_json()
    nomJoueur = bodyRqst["nomJoueur"]
    posCase = bodyRqst["case"]
    print("Coup demandé par " + str(nomJoueur) + " en position " + str(posCase))

    if (nomJoueur != joueurQuiDoitJouer):
        return 'PASTONTOUR', 241

    if (plateau[posCase] != ''):
        return 'CASEDEJAJOUEE', 242

    plateau[posCase] = nomJoueur

    if estPartieFinie():
        joueurQuiDoitJouer = "" # Plus personne ne peut jouer si la partie est finie
        return 'PARTIEGAGNEE', 230

    changerJoueur() 
    return 'COUPVALIDE', 240

@app.route('/rejouer')
def rejouer():
    global joueurQuiDoitJouer
    global plateau
    global nbJoueurs
    nbJoueurs = 0
    plateau = [
    '', '', '',
    '', '', '',
    '', '', ''
    ]
    joueurQuiDoitJouer = "X"
    return 'REJOUER', 245

