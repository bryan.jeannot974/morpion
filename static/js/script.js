"use strict";

let nomJoueur;

function reqServeur() {
    fetch('/etatPartie').then((reponse) => reponse.json().then((req) => {
        for(let pos = 0; pos < 9; pos++) {
            document.getElementById(`case${pos}`).textContent = req["plateau"][pos];
        }

        document.getElementById("quijoue").innerText = `C'est aux ${req["joueurQuiDoitJouer"]} de jouer`;
    }));
}


function traiterResCoup(reponseServeur) {
    switch (reponseServeur.status) {
        case 230:
            alert("Vous avez gagné");
            document.getElementById("StatutJeu").innerText = "Partie terminé";
            break;
        case 240: // le coup s'est bien passé, le joueur a joué
            break;
        case 241:
            alert("Pas a toi de joué");
            document.getElementById("StatutJeu").innerText = "Pas a toi de joué";
            break;
        case 242:
            alert("Cette case à déja été jouer, joue une autre");
            document.getElementById("StatutJeu").innerText = "Cette case à déja été jouer, joue une autre";
            break;
        default:
            throw new Error(`la route /jouerCoup a renvoyé un code retour inconnu : ${reponseServeur.status}`);
    }
}


function initBouttons() {
    for(let pos = 0; pos < 9; pos++) {
        document.getElementById(`case${pos}`).addEventListener("click", (event) => fetch('/jouerCoup', {
            method: 'POST',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify({nomJoueur: nomJoueur, case: pos})
          }).then(traiterResCoup));
    }
}


function traiterRejoindrePartie(dico) {
    const titre = document.getElementById("titre")
    if (!dico["entreeAcceptee"]) {
        titre.textContent = "Déjà 2 joueurs ont rejoint la partie";
        return;
    }
    titre.textContent = `Vous êtes les ${dico["lettre"]}`;
    nomJoueur = dico["lettre"];

    initBouttons();
    setInterval(reqServeur, 500);
}

fetch('/rejoindrePartie').then((reponse) => reponse.json().then(traiterRejoindrePartie));


function rejouerPartie(){
    initBouttons();
    setInterval(reqServeur, 500);
}
document.getElementById('RelancerPartie').addEventListener('click', function () {
    fetch('/rejouer')
      .then((reponse) => {
        reponse.json().then(rejouerPartie);
        return;
      })
  
  });

