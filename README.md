<div id="top"></div>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->



<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/bryan.jeannot974/morpion">
    <img src="imagesreadme/Morpion.png" alt="Logo" width="280" height="280">
  </a>

<h3 align="center">Morpion</h3>

  <p align="center">
    Création d'un morpion en Flask
    <br />
    <a href="https://gitlab.com/bryan.jeannot974/morpion"><strong>Explorer le répertoire</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/bryan.jeannot974/morpion">Voir la démonstration</a>
    ·
    <a href="issues">Reporter un bug</a>
    ·
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Sommaire</summary>
  <ol>
    <li>
      <a href="#about-the-project">A propos du projet</a>
      <ul>
        <li><a href="#built-with">Crée avec</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Pour commencer</a>
      <ul>
        <li><a href="#prerequisites">Prérequis</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Remerciements</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## A Propos du projet

[![Product Name Screen Shot][product-screenshot]](https://example.com)

Projet en 2ème année de DUT Informatique crée par`bryan.jeannot974 ` pour un projet en JavaScript.
<p align="right">(<a href="#top">Aller en haut</a>)</p>



### Crée avec


* [JavaScript](https://developer.mozilla.org/fr/docs/Web/JavaScript)

<p align="right">(<a href="#top">retour en haut de la page</a>)</p>



<!-- GETTING STARTED -->
## Pour commencer

Pour lancer l'application il faut:

* Se placer dans un dossier vierge git clone https://gitlab.com/bryan.jeannot974/morpion

* Installation

```sh
Lancer index.html sur un navigateur

```


<p align="right">(<a href="#top">retour en haut de la page</a>)</p>


<!-- CONTRIBUTING -->
## Contribution

Les contributions sont ce qui fait de la communauté open source un endroit si incroyable pour apprendre, inspirer et créer. Toutes les contributions que vous faites sont **grandement apprécié**.

Si vous avez une suggestion qui améliorerait cela, veuillez bifurquer le référentiel et créer une pull request.
N’oubliez pas de donner une étoile au projet ! Merci encore!

1. Fork le projet
2. Créer votre branche d’entités 
3. Commit les modifications 
4. Push vers la branche principale 
5. Ouvre un Pull Request

<p align="right">(<a href="#top">retour en haut de la page</a>)</p>
<p align="right">(<a href="#top">retour en haut de la page</a>)</p>





<!-- CONTACT -->
## Contact
Bryan JEANNOT - bryan.jeannot974@gmail.com

Project Link: [https://gitlab.com/bryan.jeannot974/morpion](https://gitlab.com/bryan.jeannot974/morpion)

<p align="right">(<a href="#top">retour en haut de la page</a>)</p>







<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[forks-shield]: https://img.shields.io/github/forks/github_username/repo_name.svg?style=for-the-badge
[forks-url]: -/forks
[stars-shield]: https://img.shields.io/github/stars/github_username/repo_name.svg?style=for-the-badge
[stars-url]: -/starrers
[issues-shield]: https://img.shields.io/github/issues/github_username/repo_name.svg?style=for-the-badge
[issues-url]: -/issues
[license-shield]: https://img.shields.io/github/license/github_username/repo_name.svg?style=for-the-badge
[license-url]: -/blob/main/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/linkedin_username
[product-screenshot]: imagesreadme/Screenshot.png
